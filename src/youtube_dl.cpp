/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "youtube_dl.h"

#include <QByteArray>
#include <QDebug>
#include <QJSEngine>
#include <QJSValue>
#include <QJsonDocument>
#include <QProcess>
#include <QSharedPointer>
#include <QStandardPaths>
#include <QString>
#include <QUrl>

using namespace MiTubo;

namespace MiTubo {

class YoutubeDlPrivate {
public:
    YoutubeDlPrivate();

public:
    QProcess m_downloader;
};

} // namespace

YoutubeDlPrivate::YoutubeDlPrivate()
{
}

YoutubeDl::YoutubeDl(QObject *parent):
    QObject(parent),
    d_ptr(new YoutubeDlPrivate())
{
    checkIsInstalled();
}

YoutubeDl::~YoutubeDl()
{
}

bool YoutubeDl::isInstalled() const
{
    Q_D(const YoutubeDl);
    return !d->m_downloader.program().isEmpty();
}

void YoutubeDl::checkIsInstalled()
{
    Q_D(YoutubeDl);
    bool wasInstalled = isInstalled();
    QString programPath = QStandardPaths::findExecutable("youtube-dl");
    qDebug() << "Program found:" << programPath;
    d->m_downloader.setProgram(programPath);
    if (isInstalled() != wasInstalled) {
        Q_EMIT isInstalledChanged();
    }
}

void YoutubeDl::getUrlInfo(const QUrl &url, const QJSValue &callback)
{
    Q_D(YoutubeDl);

    QSharedPointer<QMetaObject::Connection> connection(new QMetaObject::Connection);

    QJsonObject json;
    qDebug() << Q_FUNC_INFO << url;

    *connection =
        QObject::connect(&d->m_downloader,
            qOverload<int,QProcess::ExitStatus>(&QProcess::finished),
            this,
            [this, connection, callback](int exitCode,
                                         QProcess::ExitStatus exitStatus) {
            Q_D(YoutubeDl);
            QObject::disconnect(*connection);
            qDebug() << d->m_downloader.readAllStandardError();
            QJsonObject result;
            bool ok = (exitStatus == QProcess::NormalExit && exitCode == 0);
            if (ok) {
                QByteArray output = d->m_downloader.readAllStandardOutput();
                result = QJsonDocument::fromJson(output).object();
            } else {
                qWarning() << "Downloader error:" << d->m_downloader.error();
            }
            QJSValue cbCopy(callback); // needed as callback is captured as const
            QJSEngine *engine = qjsEngine(this);
            cbCopy.call(QJSValueList { engine->toScriptValue(result) });
        });

    d->m_downloader.setArguments({ "--no-check-certificate", "-j", url.toString() });
    d->m_downloader.start();
}
