import MiTubo 1.0
import QtQml 2.2
import Ubuntu.Content 1.3

QtObject {
    id: root

    property var _data: Connections {
        target: ContentHub

        onShareRequested: root.handleTransfer(transfer)
        onImportRequested: root.handleTransfer(transfer)
    }

    function handleTransfer(transfer) {
        console.log("Import requested: " + transfer.state)
        if (transfer.state === ContentTransfer.Charged) {
            transferItems(transfer)
        } else {
            transfer.stateChanged.connect(function () {
                if (transfer.state === ContentTransfer.Charged) {
                    transferItems(transfer)
                }
            })
        }
    }

    function transferItems(transfer) {
        for (var i = 0; i < transfer.items.length; i++) {
            var url = transfer.items[i].url;
            console.log("opening URL: " + url);
            RequestDispatcher.requestOpenUrl(url);
        }
    }
}
