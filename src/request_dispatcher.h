/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_REQUEST_DISPATCHER_H
#define MITUBO_REQUEST_DISPATCHER_H

#include <QObject>
#include <QScopedPointer>
#include <QString>

class QJsonObject;
class QUrl;

namespace MiTubo {

class RequestDispatcherPrivate;
class RequestDispatcher: public QObject
{
    Q_OBJECT

public:
    RequestDispatcher(QObject *parent = nullptr);
    ~RequestDispatcher();

    Q_INVOKABLE void requestOpenUrl(const QUrl &url,
                                    const QString &action = {});
    Q_INVOKABLE void requestOpenVideo(const QJsonObject &videoData,
                                      const QString &action = {});

Q_SIGNALS:
    void openUrlRequested(const QUrl &url, const QString &action);
    void openVideoRequested(const QJsonObject &videoData,
                            const QString &action);

private:
    Q_DECLARE_PRIVATE(RequestDispatcher)
    QScopedPointer<RequestDispatcherPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_REQUEST_DISPATCHER_H
