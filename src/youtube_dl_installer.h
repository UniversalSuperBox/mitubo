/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_YOUTUBE_DL_INSTALLER_H
#define MITUBO_YOUTUBE_DL_INSTALLER_H

#include <QObject>
#include <QScopedPointer>
#include <QString>

namespace MiTubo {

class YoutubeDlInstallerPrivate;
class YoutubeDlInstaller: public QObject
{
    Q_OBJECT
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString currentVersion READ currentVersion
               WRITE setCurrentVersion NOTIFY currentVersionChanged)
    Q_PROPERTY(bool autoDownload READ autoDownload
               WRITE setAutoDownload NOTIFY autoDownloadChanged)
    Q_PROPERTY(bool autoInstall READ autoInstall
               WRITE setAutoInstall NOTIFY autoInstallChanged)
    Q_PROPERTY(qint64 bytesReceived READ bytesReceived
               NOTIFY bytesReceivedChanged)
    Q_PROPERTY(qint64 bytesTotal READ bytesTotal NOTIFY bytesTotalChanged)

public:
    enum Status {
        UnknownStatus = 0,
        CheckingForUpdates,
        NoUpdatesAvailable,
        UpdateAvailable,
        CheckForUpdatesFailed,
        Downloading,
        DownloadComplete,
        DownloadFailed,
        Installing,
        InstallationComplete,
        InstallationFailed,
    };
    Q_ENUM(Status)

    YoutubeDlInstaller(QObject *parent = nullptr);
    ~YoutubeDlInstaller();

    static QString executableDir();

    Status status() const;

    void setCurrentVersion(const QString &currentVersion);
    QString currentVersion() const;

    void setAutoDownload(bool autoDownload);
    bool autoDownload() const;

    void setAutoInstall(bool autoInstall);
    bool autoInstall() const;

    int64_t bytesReceived() const;
    int64_t bytesTotal() const;

    Q_INVOKABLE void checkForUpdates();
    Q_INVOKABLE void downloadLatestVersion();
    Q_INVOKABLE void installLatestVersion();

Q_SIGNALS:
    void statusChanged();
    void currentVersionChanged();
    void autoDownloadChanged();
    void autoInstallChanged();
    void bytesReceivedChanged();
    void bytesTotalChanged();

protected:
    void setStatus(Status status);
    void setBytesReceived(int64_t bytesReceived);
    void setBytesTotal(int64_t bytesTotal);

private:
    Q_DECLARE_PRIVATE(YoutubeDlInstaller)
    QScopedPointer<YoutubeDlInstallerPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_YOUTUBE_DL_INSTALLER_H
