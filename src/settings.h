/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_SETTINGS_H
#define MITUBO_SETTINGS_H

#include <QObject>
#include <QScopedPointer>
#include <QSizeF>
#include <QStringList>

namespace MiTubo {

class SettingsPrivate;
class Settings: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList blacklistedCodecs READ blacklistedCodecs \
               WRITE setBlacklistedCodecs NOTIFY blacklistedCodecsChanged)
    Q_PROPERTY(qreal defaultVolume READ defaultVolume WRITE setDefaultVolume \
               NOTIFY defaultVolumeChanged)
    Q_PROPERTY(QSizeF windowSize READ windowSize WRITE setWindowSize
               NOTIFY windowSizeChanged)

public:
    Settings(QObject *parent = 0);
    ~Settings();

    void setBlacklistedCodecs(const QStringList &codecs);
    QStringList blacklistedCodecs() const;

    void setDefaultVolume(qreal defaultVolume);
    qreal defaultVolume() const;

    void setWindowSize(const QSizeF &size);
    QSizeF windowSize() const;

Q_SIGNALS:
    void blacklistedCodecsChanged();
    void defaultVolumeChanged();
    void windowSizeChanged();

private:
    QScopedPointer<SettingsPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Settings)
};

} // namespace

#endif // MITUBO_SETTINGS_H
