/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlist_store.h"

#include "playlist.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QJsonDocument>
#include <QStandardPaths>
#include <algorithm>

using namespace MiTubo;

namespace MiTubo {

static PlaylistStore *s_instance = nullptr;

const QString keyItems = QStringLiteral("items");
const QString keyName = QStringLiteral("name");
const QString keyPlaylists = QStringLiteral("playlists");
const QString keySortOrder = QStringLiteral("sortOrder");
const QString s_playlistDir = QStringLiteral("playlists");

class PlaylistStorePrivate {
public:
    PlaylistStorePrivate(PlaylistStore *q);

    bool savePlaylists() const;
    void loadPlaylists();
    void ensureHardcodedLists();

    void insertPlaylist(Playlist *playlist, int position = -1);

    bool savePlaylist(Playlist *playlist);

private:
    Q_DECLARE_PUBLIC(PlaylistStore)
    QDir m_baseDir;
    QList<QSharedPointer<Playlist>> m_playlists;
    PlaylistStore *q_ptr;
};

} // namespace

PlaylistStorePrivate::PlaylistStorePrivate(PlaylistStore *q):
    m_baseDir(QStandardPaths::writableLocation(
        QStandardPaths::AppDataLocation)),
    q_ptr(q)
{
    loadPlaylists();
    ensureHardcodedLists();
}

bool PlaylistStorePrivate::savePlaylists() const
{
    QFile store(m_baseDir.filePath("playlists.json"));
    if (!store.open(QIODevice::WriteOnly)) {
        qWarning() << "Could not create playlist file" << store.errorString();
        return false;
    }

    QJsonArray playlists;
    for (const auto &p: m_playlists) {
        playlists.append(p->uniqueId());
    }

    QJsonObject jsonStore {
        { keyPlaylists, playlists },
    };
    const QByteArray contents =
        QJsonDocument(jsonStore).toJson(QJsonDocument::Indented);
    int bytesWritten = store.write(contents);
    if (Q_UNLIKELY(bytesWritten < 0 || bytesWritten < contents.size())) {
        qWarning() << "Could not write playlist" << store.fileName()
            << ":" << store.errorString();
        return false;
    }

    return true;
}

void PlaylistStorePrivate::loadPlaylists()
{
    QFile store(m_baseDir.filePath("playlists.json"));
    if (!store.open(QIODevice::ReadOnly)) {
        return;
    }

    QDir playlistDir = m_baseDir;
    bool ok = playlistDir.cd(s_playlistDir);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Playlist directory not found!";
        return;
    }

    const QJsonObject jsonStore =
        QJsonDocument::fromJson(store.readAll()).object();
    const QJsonArray playlists = jsonStore.value(keyPlaylists).toArray();
    for (const QJsonValue &v: playlists) {
        QString playlistUniqueId = v.toString();
        QFile playlistFile(playlistDir.filePath(playlistUniqueId));
        ok = playlistFile.open(QIODevice::ReadOnly);
        if (Q_UNLIKELY(!ok)) {
            qWarning() << "Playlist not found:" << playlistUniqueId;
            continue;
        }

        const QJsonObject playlistData =
            QJsonDocument::fromJson(playlistFile.readAll()).object();
        const QString playlistName =
            playlistData.value(keyName).toString();
        const Playlist::SortOrder sortOrder =
            static_cast<Playlist::SortOrder>(
                playlistData.value(keySortOrder).toInt());
        const QJsonArray playlistItems =
            playlistData.value(keyItems).toArray();
        insertPlaylist(new Playlist(playlistUniqueId, playlistName,
                                    sortOrder, playlistItems));
    }
}

void PlaylistStorePrivate::ensureHardcodedLists()
{
    Q_Q(PlaylistStore);

    bool playlistsAdded = false;

    const QStringList hardcodedLists {
        Playlist::continueWatchingListId(),
        Playlist::watchHistoryListId(),
    };
    for (auto listIter = hardcodedLists.begin();
         listIter != hardcodedLists.end();
         listIter++) {
        const QString &listId = *listIter;
        auto i = std::find_if(m_playlists.constBegin(),
                              m_playlists.constEnd(),
                              [&listId](const QSharedPointer<Playlist> &p) {
            return p->uniqueId() == listId;
        });
        if (i != m_playlists.constEnd()) continue;

        Playlist *playlist =
            new Playlist(listId, q->tr(listId.toUtf8().constData()),
                         Playlist::NewerFirst, {});
        insertPlaylist(playlist, listIter - hardcodedLists.begin());
        playlistsAdded = true;
    }

    if (playlistsAdded) {
        savePlaylists();
    }
}

void PlaylistStorePrivate::insertPlaylist(Playlist *playlist, int position)
{
    Q_Q(PlaylistStore);
    QSharedPointer<Playlist> playlistPtr(playlist);
    if (position < 0) {
        m_playlists.append(playlistPtr);
    } else {
        m_playlists.insert(position, playlistPtr);
    }
    QObject::connect(playlist, &Playlist::itemsChanged,
                     q, [this, q, playlist]() {
        savePlaylist(playlist);
        Q_EMIT q->playlistsChanged();
    });
    Q_EMIT q->playlistsChanged();
}

bool PlaylistStorePrivate::savePlaylist(Playlist *playlist)
{
    bool ok = m_baseDir.mkpath(s_playlistDir);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Error creating playlist directory";
        return false;
    }

    QDir playlistDir(m_baseDir);
    ok = playlistDir.cd(s_playlistDir);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not access directory (wrong permissions?)" <<
            playlistDir.filePath(s_playlistDir);
        return false;
    }

    QFile playlistFile(playlistDir.filePath(playlist->uniqueId()));
    ok = playlistFile.open(QIODevice::WriteOnly);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open playlist file for writing" <<
            playlistFile.errorString();
        return false;
    }

    QJsonObject playlistData {
        { keyName, playlist->name() },
        { keySortOrder, int(playlist->sortOrder()) },
        { keyItems, playlist->items() },
    };

    QByteArray contents =
        QJsonDocument(playlistData).toJson(QJsonDocument::Indented);
    int bytesWritten = playlistFile.write(contents);
    if (Q_UNLIKELY(bytesWritten < 0 || bytesWritten < contents.size())) {
        qWarning() << "Could not write playlist" << playlistFile.fileName()
            << ":" << playlistFile.errorString();
        return false;
    }

    return true;
}

PlaylistStore::PlaylistStore(QObject *parent):
    QObject(parent),
    d_ptr(new PlaylistStorePrivate(this))
{
}

PlaylistStore::~PlaylistStore() = default;

PlaylistStore *PlaylistStore::instance()
{
    if (!s_instance) {
        s_instance = new PlaylistStore();
    }
    return s_instance;
}

QList<QSharedPointer<Playlist>> PlaylistStore::playlists() const
{
    Q_D(const PlaylistStore);
    return d->m_playlists;
}

void PlaylistStore::insertPlaylist(Playlist *playlist, int position)
{
    Q_D(PlaylistStore);
    if (d->savePlaylist(playlist)) {
        d->insertPlaylist(playlist, position);
        d->savePlaylists();
    }
}

void PlaylistStore::deletePlaylist(const QString &name)
{
    Q_D(PlaylistStore);
    for (auto i = d->m_playlists.begin(); i != d->m_playlists.end(); i++) {
        if ((*i)->name() == name) {
            d->m_playlists.erase(i);
            break; // we know there are no duplicates
        }
    }
    d->savePlaylists();
    Q_EMIT playlistsChanged();
}
