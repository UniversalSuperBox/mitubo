import QtQuick 2.7
import QtQuick.Controls 2.2

Item {
    id: root

    property alias message: label.text

    anchors.fill: parent
    visible: false

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.5
    }

    BusyIndicator {
        id:indicator
        anchors.centerIn: parent
    }

    Label {
        id: label
        anchors {
            top: indicator.bottom; topMargin: 12
            left: parent.left; right: parent.right
        }
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        color: "#ddd"
    }
}
