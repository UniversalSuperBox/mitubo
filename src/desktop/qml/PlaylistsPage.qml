import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    property var playlistModel: null

    signal playRequested(var videoData)
    signal downloadRequested()

    title: qsTr("Playlists")

    header: TitleHeader {}

    ListView {
        ScrollBar.vertical: ScrollBar {}
        anchors.fill: parent
        model: root.playlistModel
        delegate: PlaylistDelegate {
            x: 12
            width: ListView.view.width - 24
            text: model.name
            subText: qsTr("%2 elements", "", model.count).arg(model.count)
            onClicked: root.openPlaylist(model.playlist)
            onDeletionRequested: playlistModel.deletePlaylist(index)
        }
        footer: PlaylistItemNew {
            x: 4
            width: parent.width - 8
            onPlaylistNameChosen: playlistModel.createPlaylist(playlistName, [])
        }
    }

    function openPlaylist(playlist) {
        var pageStack = StackView.view
        var page = pageStack.push(Qt.resolvedUrl("PlaylistPage.qml"), {
            "playlist": playlist,
        })
        page.playRequested.connect(root.playRequested)
    }
}
