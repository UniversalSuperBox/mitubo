import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    signal finished()

    title: qsTr("youtube-dl installation")

    ColumnLayout {
        anchors { fill: parent; margins: 8 }

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 400
            Layout.alignment: Qt.AlignHCenter
            textFormat: Text.StyledText
            text: qsTr("\
<p>MiTubo relies on the <b>youtube-dl</b> program to perform the \
extraction of the video streams.</p>\
<p>A working installation of <b>youtube-dl</b> could not be found on your \
system. MiTubo will now start the installation process.")
            wrapMode: Text.WordWrap
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Install youtube-dl")
            highlighted: true
            onClicked: root.pushInstallPage()
        }
    }

    function pushInstallPage() {
        var page =
            pageStack.push(Qt.resolvedUrl("DownloaderInstallationPage.qml"), {
        })
        page.finished.connect(function() {
            root.StackView.view.pop()
            root.finished()
        })
    }
}
