import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.2
import MiTubo 1.0

ApplicationWindow {
    id: root

    width: Settings.windowSize.width
    height: Settings.windowSize.height
    visible: true

    property bool youtubeDlRunning: false

    Component.onCompleted: {
        pageStack.forceActiveFocus()
        var args = Qt.application.arguments
        for (var i = 1; i < args.length; i++) {
            console.log("Got arg: " + args[i])
            if (args[i][0] != '-') {
                RequestDispatcher.requestOpenUrl(args[i]);
            }
        }
    }
    onActiveFocusControlChanged: console.log("Active focus control: " + activeFocusControl)
    onActiveFocusItemChanged: console.log("Active focus item: " + activeFocusItem)

    PlatformInitialization {}

    StackView {
        id: pageStack
        anchors.fill: parent
        initialItem: downloader.installed ?
            mainPageComponent : downloaderIntroPage
    }

    MainPlaylistModel { id: playlistModel }

    Component {
        id: mainPageComponent
        MainPage {
            onUrlAdded: RequestDispatcher.requestOpenUrl(videoUrl)
            onSearchRequested: startSearch(searchText)
            onPlaylistPageRequested: {
                var page = pageStack.push(Qt.resolvedUrl("PlaylistsPage.qml"),
                                          { 'playlistModel': playlistModel })
                page.playRequested.connect(function (videoData) {
                    RequestDispatcher.requestOpenVideo(videoData)
                })
            }
        }
    }

    Component {
        id: downloaderIntroPage
        DownloaderIntroPage {
            onFinished: {
                downloader.checkIsInstalled()
                pageStack.push(mainPageComponent)
            }
        }
    }

    BusyOverlay {
        id: busyOverlay
        visible: youtubeDlRunning
        message: qsTr("Retrieving stream information from the web page…")
    }

    YoutubeDl {
        id: downloader
    }

    Connections {
        target: RequestDispatcher
        onOpenUrlRequested: root.openUrl(url, action)
        onOpenVideoRequested: root.openVideo(videoData, action)
    }

    Connections {
        target: Qt.application
        onAboutToQuit: Settings.windowSize = Qt.size(root.width, root.height)
    }

    function showVideoInfo(json) {
        var page = pageStack.push(Qt.resolvedUrl("VideoInfoPage.qml"), {
            "videoData": json,
            "playlistModel": playlistModel,
        })
        page.playRequested.connect(function () { startPlayer(json) })
    }

    function startPlayer(json) {
        pageStack.push(Qt.resolvedUrl("PlayerScreen.qml"), {
            "videoData": json,
            "playlistModel": playlistModel,
        })
    }

    function onUrlOpened(json, action) {
        youtubeDlRunning = false
        if (action == "play") {
            startPlayer(json)
        } else if (action == "info") {
            showVideoInfo(json)
        }
    }

    function openVideo(videoData, action) {
        youtubeDlRunning = true
        if (!action) action = "play"
        downloader.getUrlInfo(videoData.webpage_url, function (json) {
            // merge the two objects
            for (var key in json) {
                videoData[key] = json[key]
            }
            onUrlOpened(videoData, action)
        })
    }

    function openUrl(url, action) {
        youtubeDlRunning = true
        if (!action) action = "play"
        downloader.getUrlInfo(url, function (json) {
            onUrlOpened(json, action)
        })
    }

    function startSearch(searchText) {
        var page = pageStack.push(Qt.resolvedUrl("SearchPage.qml"), {
            "searchText": searchText,
        })
        page.urlAdded.connect(function(videoUrl, action) {
            RequestDispatcher.requestOpenUrl(videoUrl, action)
        })
    }
}
