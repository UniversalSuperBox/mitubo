import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    signal finished()

    title: qsTr("youtube-dl installation")

    states: [
        State {
            name: "CheckingForUpdates"
            when: installer.status == YoutubeDlInstaller.CheckingForUpdates
            PropertyChanges { target: msg; text: qsTr("Checking for updates…") }
        },
        State {
            name: "NoUpdatesAvailable"
            when: installer.status == YoutubeDlInstaller.NoUpdatesAvailable
            PropertyChanges { target: msg; text: qsTr("No download available") }
        },
        State {
            name: "CheckForUpdatesFailed"
            when: installer.status == YoutubeDlInstaller.CheckingForUpdates
            PropertyChanges { target: msg; text: qsTr("Failed. Please check your network connection.") }
        },
        State {
            name: "Downloading"
            when: installer.status == YoutubeDlInstaller.Downloading
            PropertyChanges { target: msg; text: qsTr("Downloading…") }
            PropertyChanges { target: downloadProgressBar; visible: true }
        },
        State {
            name: "DownloadFailed"
            when: installer.status == YoutubeDlInstaller.DownloadFailed
            PropertyChanges { target: msg; text: qsTr("Download failed. Please check your network connection.") }
        },
        State {
            name: "Installing"
            when: installer.status == YoutubeDlInstaller.Installing
            PropertyChanges { target: msg; text: qsTr("Installing…") }
        },
        State {
            name: "InstallationComplete"
            when: installer.status == YoutubeDlInstaller.InstallationComplete
            PropertyChanges { target: msg; text: qsTr("Installation complete!") }
            PropertyChanges { target: doneButton; visible: true }
        },
        State {
            name: "InstallationFailed"
            when: installer.status == YoutubeDlInstaller.InstallationFailed
            PropertyChanges { target: msg; text: qsTr("Installation failed") }
        }
    ]

    ColumnLayout {
        anchors { fill: parent; margins: 8 }

        Label {
            id: msg
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
        }

        ProgressBar {
            id: downloadProgressBar
            Layout.fillWidth: true
            visible: false
            to: installer.bytesTotal
            value: installer.bytesReceived
        }

        Button {
            id: doneButton
            Layout.alignment: Qt.AlignHCenter
            visible: false
            text: qsTr("Done")
            onClicked: {
                root.StackView.view.pop()
                root.finished()
            }
        }
    }

    YoutubeDlInstaller {
        id: installer
        onStatusChanged: console.log("Installer status: " + status)
        autoDownload: true
        Component.onCompleted: checkForUpdates()
    }

}
