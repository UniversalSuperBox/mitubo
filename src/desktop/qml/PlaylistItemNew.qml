import QtQuick 2.7
import QtQuick.Controls 2.2

ItemDelegate {
    id: root

    signal playlistNameChosen(string playlistName)

    text: qsTr("New list...")
    onClicked: listNameDialog.open()

    Dialog {
        id: listNameDialog
        margins: 10
        y: visible, -nameField.mapToItem(background, 0, 0).y
        title: qsTr("Create a new playlist")
        focus: true
        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: root.playlistNameChosen(nameField.text)

        TextFieldWithContextMenu {
            id: nameField
            anchors.fill: parent
            focus: true
            placeholderText: qsTr("Enter the list name, e.g. \"Watch later\"")
            onAccepted: listNameDialog.accept()
        }
    }
}
