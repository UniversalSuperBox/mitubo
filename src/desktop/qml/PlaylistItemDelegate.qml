import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

SwipeDelegate {
    id: root

    property alias title: titleLabel.text
    property alias description: descriptionLabel.text
    property int duration: 0
    property alias host: hostLabel.text
    property alias previewUrl: preview.source
    property url pageUrl

    signal playRequested()
    signal deletionRequested()

    swipe.right: ToolButton {
        anchors {
            top: parent.top; bottom: parent.bottom
            left: parent.background.right
        }
        text: qsTr("Delete")
        onClicked: root.deletionRequested()
    }
    clip: true
    contentItem: RowLayout {
        id: layout

        Image {
            id: preview
            Layout.minimumWidth: 100
            Layout.maximumWidth: 100
            sourceSize.width: 100
            fillMode: Image.PreserveAspectFit
        }

        GridLayout {
            Layout.fillWidth: true
            columns: 2
            columnSpacing: 2

            Label {
                id: titleLabel
                Layout.fillWidth: true
                Layout.columnSpan: 2
                elide: Text.ElideRight
            }

            Label {
                id: descriptionLabel
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.columnSpan: 2
                maximumLineCount: 3
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
            }

            Rectangle {
                radius: 8
                implicitWidth: durationLabel.implicitWidth + 16
                implicitHeight: durationLabel.implicitHeight + 4
                color: "#333"
                Label {
                    id: durationLabel
                    anchors { fill: parent; leftMargin: 8; rightMargin: 8 }
                    text: Utils.formatMediaTime(root.duration * 1000)
                    color: "white"
                    font.pointSize: 8
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
            Label {
                id: hostLabel
                Layout.fillWidth: true
            }
        }

        ColumnLayout {
            Button {
                text: qsTr("Play")
                highlighted: true
                onClicked: root.playRequested()
            }
        }
    }
}
