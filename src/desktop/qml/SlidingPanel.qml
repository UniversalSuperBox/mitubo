import QtQuick 2.7

OsdPanel {
    id: root

    property string position: "top"

    property int yWhenOpened: position == "top" ? 0 : (parent.height - height)
    anchors.left: parent.left
    anchors.right: parent.right
    y: position == "top" ? -height : parent.height

    states: [
        State {
            name: "exposed"
            PropertyChanges { target: root; y: yWhenOpened }
        }
    ]


    transitions: [
        Transition {
            NumberAnimation { properties: "y"; duration: 300 }
        }
    ]
}
