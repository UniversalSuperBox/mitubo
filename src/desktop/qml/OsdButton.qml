import QtQuick 2.7
import "Constants.js" as UI

MouseArea {
    id: root

    property alias icon: image.source

    height: UI.OsdButtonHeight
    width: height

    Image {
        id: image
        anchors.fill: parent
        sourceSize { width: width; height: height }
        smooth: true
    }
}
