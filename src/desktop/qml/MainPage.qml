import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    signal playlistPageRequested()
    signal searchRequested(string searchText)
    signal urlAdded(url videoUrl)

    title: qsTr("Video lookup")

    ColumnLayout {
        anchors { left: parent.left; right: parent.right; margins: 8 }

        Label {
            Layout.fillWidth: true
            text: qsTr("Enter some search terms or the address of the page containing the desired video")
            wrapMode: Text.WordWrap
        }

        TextFieldWithContextMenu {
            id: urlField
            property bool isUrl: text.startsWith("http")
            Layout.fillWidth: true
            placeholderText: qsTr("Video address")
            selectByMouse: true
            onAccepted: activate()

            function activate() {
                if (isUrl) root.urlAdded(urlField.text)
                else root.searchRequested(urlField.text)
            }
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: urlField.isUrl ? qsTr("Play") : qsTr("Search")
            highlighted: true
            onClicked: urlField.activate()
        }

        Label {
            Layout.topMargin: 50
            Layout.bottomMargin: 10
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("or")
            visible: playlistButton.visible
        }

        Button {
            id: playlistButton
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Go to your playlists")
            onClicked: root.playlistPageRequested()
        }
    }
}
