import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    property var playlistModel: null
    property var videoData: ({})

    signal playRequested()
    signal downloadRequested()

    title: qsTr("Video details")

    header: TitleHeader {}

    Flickable {
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height + layout.anchors.margins * 2

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: layout

            anchors {
                margins: 8; top: parent.top
                left: parent.left; right: parent.right
            }

            Label {
                id: titleLabel
                Layout.fillWidth: true
                text: videoData.title
                wrapMode: Text.Wrap
                font { bold: true; pointSize: 20 }
                visible: videoData.title
            }

            Label {
                Layout.fillWidth: true
                visible: !titleLabel.visible
                text: qsTr("Failed to retrieve title")
                font { italic: true; pointSize: 16 }
                color: "red"
            }

            GridLayout {
                Layout.fillWidth: true
                flow: parent.width > 500 ? GridLayout.LeftToRight : GridLayout.TopToBottom
                columnSpacing: 16
                rowSpacing: 8

                Image {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 10 // Let buttons take precedence
                    Layout.maximumHeight: 200
                    Layout.margins: 8
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    sourceSize { height: 200; width: 300 }
                    source: videoData.thumbnail
                }

                GridLayout {
                    property bool vertical: parent.flow == GridLayout.LeftToRight
                    Layout.fillWidth: vertical
                    Layout.alignment: vertical ? Qt.AlignLeft : Qt.AlignHCenter
                    flow: vertical ? GridLayout.TopToBottom : GridLayout.LeftToRight

                    Button {
                        Layout.fillWidth: true
                        Layout.maximumWidth: 300
                        highlighted: true
                        text: qsTr("Play")
                        onClicked: root.playRequested()
                    }

                    Button {
                        id: watchLaterButton
                        Layout.fillWidth: true
                        Layout.maximumWidth: 300
                        text: qsTr("Watch later")
                        onClicked: watchLater.open()
                    }

                    Button {
                        Layout.fillWidth: true
                        Layout.maximumWidth: 300
                        text: qsTr("Download")
                        onClicked: root.downloadRequested()
                        enabled: false // TODO until implemented
                    }
                }

                WatchLaterPanel {
                    id: watchLater
                    parent: watchLaterButton
                    playlistModel: root.playlistModel
                    onAddToPlaylistRequested: list.addVideo(root.videoData)
                }
            }

            Frame {
                Layout.fillWidth: true
                implicitWidth: 10

                GridLayout {
                    anchors.fill: parent
                    columnSpacing: 16
                    columns: (width > uploaderLabel.contentWidth + durationLabel.contentWidth +
                                      dateLabel.contentWidth + viewsLabel.contentWidth + 100) ? 4 : 2

                    Label {
                        id: uploaderLabel
                        Layout.fillWidth: true
                        text: (videoData.uploader_url ?
                            qsTr('By <b><a href="%1">%2</a></b>').arg(videoData.uploader_url) :
                            qsTr('By <b>%1</b>')).arg(videoData.uploader)
                        textFormat: Text.StyledText
                        onLinkActivated: Qt.openUrlExternally(link)
                    }

                    Label {
                        id: durationLabel
                        Layout.fillWidth: true
                        text: qsTr('Duration: %1').arg(Utils.formatMediaTime(videoData.duration * 1000))
                    }

                    Label {
                        id: dateLabel
                        Layout.fillWidth: true
                        property var uploadDate: layout.uploadDate()
                        text: uploadDate ? qsTr("Uploaded on %1").arg(Qt.formatDate(uploadDate)) :
                                           qsTr("Upload date unknown")
                    }

                    Label {
                        id: viewsLabel
                        Layout.fillWidth: true
                        text: videoData.view_count ?
                            qsTr('%1 views', '', videoData.view_count).arg(videoData.view_count) :
                            qsTr('View count unknown')
                    }
                }
            }

            Label {
                Layout.fillWidth: true
                Layout.topMargin: 16
                text: qsTr("Description")
                font { bold: true; pointSize: 14 }
                visible: videoData.description
            }

            Label {
                Layout.fillWidth: true
                wrapMode: Text.Wrap
                text: videoData.description
            }

            Label {
                Layout.fillWidth: true
                Layout.topMargin: 16
                text: qsTr('<a href="%1">View web page in browser</a>').arg(videoData.webpage_url)
                textFormat: Text.StyledText
                onLinkActivated: Qt.openUrlExternally(link)
            }

            function uploadDate() {
                var dateString = videoData.upload_date
                if (!dateString) return null

                var ret = dateString.substring(0, 4) + '-' +
                    dateString.substring(4, 6) + '-' +
                    dateString.substring(6, 8)
                return ret
            }
        }
    }
}
