import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Column {
    id: root

    readonly property bool interacting: settings.opened ||
        watchLater.opened || volumeControl.interacting
    property bool playing: false
    property int duration: 0
    property int position: 0
    property alias volume: volumeControl.value
    property var formats: []
    property alias currentFormatIndex: settings.currentFormatIndex
    property var videoData: ({})

    signal formatRequested(var formatIndex)
    signal seekRequested(int seekPosition)
    signal togglePlayRequested()
    signal toggleFullScreenRequested()
    signal volumeRequested(real volume)
    signal playbackRateRequested(real rate)

    spacing: 4

    VideoProgressBar {
        id: progressBar
        anchors { left: parent.left; right: parent.right }
        duration: root.duration
        position: root.position
        onSeekRequested: root.seekRequested(seekPosition)
    }

    RowLayout {
        anchors { left: parent.left; right: parent.right }
        spacing: 4

        OsdButton {
            icon: root.playing ? "qrc:/icons/pause" : "qrc:/icons/play"
            onClicked: root.togglePlayRequested()
        }

        Label {
            Layout.fillWidth: true
            color: "white"
            font.pixelSize: parent.height / 2
            text: qsTr("%1 / %2").arg(Utils.formatMediaTime(root.position))
                                 .arg(Utils.formatMediaTime(root.duration))
        }

        VolumeControl {
            id: volumeControl
            onVolumeRequested: root.volumeRequested(volume)
        }

        OsdButton {
            icon: "qrc:/icons/playlist-add"
            onClicked: watchLater.open()

            WatchLaterPanel {
                id: watchLater
                y: -(height + bottomMargin)
                onAddToPlaylistRequested: list.addVideo(root.videoData, root.position)
            }
        }

        OsdButton {
            icon: "qrc:/icons/settings"
            onClicked: settings.open()

            VideoSettingsPanel {
                id: settings
                y: -root.height
                formats: root.formats
                onFormatRequested: root.formatRequested(formatIndex)
                onPlaybackRateRequested: root.playbackRateRequested(rate)
            }
        }

        OsdButton {
            icon: "qrc:/icons/fullscreen"
            onClicked: root.toggleFullScreenRequested()
        }
    }
}
