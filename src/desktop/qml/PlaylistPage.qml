import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    property var playlist: null

    signal playRequested(var videoData)

    title: qsTr("Playlist " + playlist.name)

    header: TitleHeader {
        ToolButton {
            id: sortButton
            icon {
                source: "qrc:/icons/playlist-sort"
            }
            onClicked: sortPopup.open()
        }
    }

    ListView {
        ScrollBar.vertical: ScrollBar {}
        anchors.fill: parent
        model: playlist.items
        delegate: PlaylistItemDelegate {
            x: 12
            width: parent.width - 24
            title: modelData.title
            description: modelData.description || ""
            duration: modelData.duration
            host: modelData.host || ""
            previewUrl: modelData.thumbnail
            pageUrl: modelData.webpage_url
            onPlayRequested: root.playRequested(modelData)
            onDeletionRequested: root.deleteItem(index)
        }
    }

    Popup {
        id: sortPopup
        parent: sortButton
        margins: 1
        y: parent.height

        property var model: [
            { "label": qsTr("Newer first"), "order": Playlist.NewerFirst },
            { "label": qsTr("Older first"), "order": Playlist.OlderFirst },
        ]
        ColumnLayout {
            Repeater {
                model: sortPopup.model
                Button {
                    Layout.fillWidth: true
                    text: modelData.label
                    onClicked: sortPopup.sort(modelData.order)
                    flat: true
                }
            }
        }

        function sort(order) {
            root.playlist.sortOrder = order
            sortPopup.close()
        }
    }

    function deleteItem(index) {
        playlist.takeAt(index)
    }
}
