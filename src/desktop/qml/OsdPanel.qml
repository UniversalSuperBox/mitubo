import QtQuick 2.7

Item {
    id: root

    default property alias content: contentItem.data
    property int padding: 0
    property alias cornerRadius: background.radius

    width: contentItem.implicitWidth
    height: contentItem.implicitHeight

    Rectangle {
        id: background
        anchors.fill: parent
        color: "#222"
        opacity: 0.8
    }

    Item {
        id: contentItem
        anchors { fill: parent; margins: root.padding }
        implicitWidth: children[0].implicitWidth + anchors.leftMargin + anchors.rightMargin
        implicitHeight: children[0].implicitHeight + anchors.bottomMargin + anchors.topMargin
    }
}
