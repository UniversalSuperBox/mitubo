import MiTubo 1.0
import QtMultimedia 5.8
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Window 2.2

Rectangle {
    id: root

    property int formatIndex: -1
    property var videoData: null
    property var blacklistedCodecs: Settings.blacklistedCodecs
    property var playlistModel: null
    readonly property bool playing: mediaPlayer.playbackState == MediaPlayer.PlayingState

    color: "black"
    onBlacklistedCodecsChanged: filterFormats()
    Keys.onPressed: {
        switch (event.key) {
        case Qt.Key_F: toggleFullScreen(); break
        case Qt.Key_Space: mediaPlayer.togglePlaying(); break
        }
    }

    property var _formats: videoData.formats
    property int _selectedFormatIndex: formatIndex >= 0 ? formatIndex : pickBestFormat()
    property var _selectedFormat: _formats[_selectedFormatIndex]

    MediaPlayer {
        id: mediaPlayer
        autoPlay: true
        source: _selectedFormat.url
        volume: Settings.defaultVolume
        onSourceChanged: if (position <= 0) setStartingPosition()
        onVolumeChanged: volumeTimer.restart()
        onError: root.handleError(error, errorString)

        function togglePlaying() {
            if (playbackState == MediaPlayer.PlayingState) {
                pause()
            } else {
                play()
            }
        }

        function setStartingPosition() {
            if (root.videoData.startTime > 0) {
                seek(root.videoData.startTime)
            }
        }
    }

    VideoOutput {
        anchors.fill: parent
        source: mediaPlayer

        SlidingPanel {
            position: "top"
            padding: 4
            state: panelController.effectiveState
            VideoTopRow {
                anchors.fill: parent
                videoTitle: videoData.title
                onBackRequested: root.goBack()
            }
        }

        SlidingPanel {
            position: "bottom"
            padding: 4
            state: panelController.effectiveState
            VideoBottomRow {
                id: videoBottomRow
                anchors.fill: parent
                formats: _formats
                currentFormatIndex: _selectedFormatIndex
                videoData: root.videoData
                playing: root.playing
                duration: mediaPlayer.duration
                position: mediaPlayer.position
                volume: mediaPlayer.volume
                onSeekRequested: mediaPlayer.seek(seekPosition)
                onTogglePlayRequested: mediaPlayer.togglePlaying()
                onToggleFullScreenRequested: root.toggleFullScreen()
                onFormatRequested: root.playFormat(formatIndex)
                onVolumeRequested: mediaPlayer.volume = volume
                onPlaybackRateRequested: mediaPlayer.playbackRate = rate
            }
        }

        FadingPanel {
            anchors.centerIn: parent
            padding: 16
            cornerRadius: 16
            state: panelController.effectiveState
            AbstractButton {
                implicitWidth: 100
                implicitHeight: 100
                indicator: Image {
                    anchors.fill: parent
                    source: root.playing ? "qrc:/icons/pause" : "qrc:/icons/play"
                    sourceSize { width: width; height: height }
                }
                onClicked: mediaPlayer.togglePlaying()
            }
        }

        HidingPanelController {
            id: panelController
            anchors.fill: parent
            forceOpen: !root.playing || videoBottomRow.interacting
        }
    }

    Timer {
        id: volumeTimer
        interval: 2000
        onTriggered: Settings.defaultVolume = mediaPlayer.volume
    }

    Loader {
        id: dialogLoader
        anchors.fill: parent
    }

    function addVideoToAutomaticList() {
        if (mediaPlayer.status == MediaPlayer.NoMedia ||
            mediaPlayer.status == MediaPlayer.InvalidMedia) {
            // nothing to do
            return
        }
        if (mediaPlayer.status == MediaPlayer.EndOfMedia ||
            (mediaPlayer.duration > 0 &&
             mediaPlayer.position > mediaPlayer.duration - 30000)) {
            playlistModel.addWatchedVideo(videoData)
        } else {
            playlistModel.addToContinueWatching(videoData, mediaPlayer.position)
        }
    }

    function goBack() {
        addVideoToAutomaticList()
        mediaPlayer.stop()
        root.StackView.view.pop()
    }

    function toggleFullScreen() {
        var window = Window.window
        if (window.visibility == Window.FullScreen) {
            window.showNormal()
        } else {
            window.showFullScreen()
        }
    }

    function playFormat(formatIndex) {
        var playerStatus = mediaPlayer.playbackState
        var position = mediaPlayer.position
        _selectedFormatIndex = formatIndex
        console.log("Play format switched to " + JSON.stringify(_selectedFormat, null, 2))
        mediaPlayer.seek(position)
        if (playerStatus == MediaPlayer.PlayingState) {
            mediaPlayer.play()
        } else if (playerStatus == MediaPlayer.PausedState) {
            mediaPlayer.pause()
        }
    }

    function handleError(code, message) {
        console.log("MediaPlayer error (" + code + "):" + message)
        if (code == MediaPlayer.FormatError) {
            dialogLoader.setSource(Qt.resolvedUrl("UnsupportedCodecPopup.qml"), {
                "format": _selectedFormat,
            });
            dialogLoader.item.open()
            dialogLoader.item.blacklistRequested.connect(blacklistCodec)
        }
    }

    function blacklistCodec(codec) {
        console.log("Blacklisting codec:" + codec)
        var tmp = blacklistedCodecs
        tmp.push(codec)
        Settings.blacklistedCodecs = tmp
    }

    function codecIsValid(codec) {
        return codec && codec != "none";
    }

    function codecIsUnsupported(codec) {
        return blacklistedCodecs.indexOf(codec) >= 0;
    }

    function filterFormats() {
        var formats = _formats;
        var supportedFormats = [];
        for (var i = 0; i < formats.length; i++) {
            var format = formats[i];
            if (codecIsValid(format.acodec) &&
                codecIsUnsupported(format.acodec)) continue;
            if (codecIsValid(format.vcodec) &&
                codecIsUnsupported(format.vcodec)) continue;
            supportedFormats.push(format);
        }
        _formats = supportedFormats;
    }

    function pickBestFormat() {
        var formats = _formats;
        var audioVideoFormats = [];
        var audioOnlyFormats = [];
        var videoOnlyFormats = [];
        for (var i = 0; i < formats.length; i++) {
            var format = formats[i];
            if (format.acodec != "none" && format.vcodec != "none") {
                audioVideoFormats.push(i);
            } else if (format.acodec != "none") {
                audioOnlyFormats.push(i);
            } else {
                videoOnlyFormats.push(i);
            }
        }
        if (audioVideoFormats.length > 0) {
            return audioVideoFormats[0];
        } else if (audioOnlyFormats.length > 0) {
            return audioOnlyFormats[0];
        } else if (videoOnlyFormats.length > 0) {
            return videoOnlyFormats[0];
        } else {
            console.warn("No audio or video formats found")
            return 0;
        }
    }
}
