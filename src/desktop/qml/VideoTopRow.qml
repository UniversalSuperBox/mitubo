import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

RowLayout {
    id: root

    property string videoTitle: ""

    signal backRequested()

    spacing: 4

    OsdButton {
        icon: "qrc:/icons/back"
        onClicked: root.backRequested()
    }

    Label {
        Layout.fillWidth: true
        color: "white"
        font.pointSize: 16
        text: videoTitle
        elide: Text.ElideRight
    }
}
