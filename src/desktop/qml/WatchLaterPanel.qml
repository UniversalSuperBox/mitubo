import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var playlistModel: null

    signal addToPlaylistRequested(var list)

    margins: 8

    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            text: qsTr("Add to playlist:")
        }

        ListView {
            id: streamSelector
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 200
            ScrollBar.vertical: ScrollBar {}
            implicitWidth: contentItem.childrenRect.width
            implicitHeight: contentItem.childrenRect.height
            clip: true
            model: root.playlistModel
            delegate: PlaylistDelegate {
                editable: false
                text: model.name
                subText: qsTr("%2 elements", "", model.count).arg(model.count)
                onClicked: root.addToList(model.name)
            }
            footer: PlaylistItemNew {
                onPlaylistNameChosen: {
                    root.playlistModel.createPlaylist(playlistName, [root.videoData])
                    root.close()
                }
            }
        }
    }

    function addToList(name) {
        var list = playlistModel.playlistByName(name)
        root.addToPlaylistRequested(list)
        root.close()
    }
}
