import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var formats: []
    property int currentFormatIndex: -1

    signal formatRequested(int formatIndex)
    signal playbackRateRequested(real rate)

    property var _streamModel: []
    /* This array contains, at each index `i`, the list of formats for the
     * stream configuration at _streamModel[i] */
    property var _formatsPerStream: []
    property var _speedModel: [
        { "label": qsTr("Half speed"), "speed": 0.5 },
        { "label": qsTr("Normal speed"), "speed": 1.0 },
        { "label": qsTr("1.25x"), "speed": 1.25 },
        { "label": qsTr("1.5x"), "speed": 1.5 },
        { "label": qsTr("Double speed"), "speed": 2.0 },
        { "label": qsTr("Quadruple speed"), "speed": 4.0 },
    ]

    margins: 8
    onFormatsChanged: prepareModels()

    GridLayout {
        anchors.fill: parent
        columns: 2

        Label {
            Layout.fillWidth: true
            text: qsTr("Streams")
        }

        ComboBox {
            id: streamSelector
            Layout.minimumWidth: implicitWidth * 2
            Layout.fillWidth: true
            model: _streamModel
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Format")
        }

        ComboBox {
            id: formatSelector
            Layout.fillWidth: true
            model: _formatsPerStream[streamSelector.currentIndex]
            textRole: "label"
            onActivated: root.selectFormat(model[currentIndex])
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Playback speed")
        }

        ComboBox {
            id: speedSelector
            Layout.fillWidth: true
            model: _speedModel
            textRole: "label"
            currentIndex: 1
            onActivated: root.playbackRateRequested(model[currentIndex].speed)
        }
    }

    function selectActiveFormat() {
        for (var i = 0; i < _formatsPerStream.length; i++) {
            var formats = _formatsPerStream[i]
            for (var j = 0; j < formats.length; j++) {
                var format = formats[j]
                if (format["index"] == currentFormatIndex) {
                    streamSelector.currentIndex = i
                    formatSelector.currentIndex = j
                    return
                }
            }
        }
    }

    function prepareModels() {
        var audioVideoFormats = []
        var audioOnlyFormats = []
        var videoOnlyFormats = []
        for (var i = 0; i < formats.length; i++) {
            var format = formats[i]
            format["label"] = qsTr("%1 (bitrate: %2)").arg(format.format_note).arg(format.tbr)
            format["index"] = i
            if (format.acodec != "none" && format.vcodec != "none") {
                audioVideoFormats.push(format)
            } else if (format.acodec != "none") {
                audioOnlyFormats.push(format)
            } else {
                videoOnlyFormats.push(format)
            }
        }
        var streamModel = []
        var formatsPerStream = []
        if (audioVideoFormats.length > 0) {
            streamModel.push(qsTr("Audio + video"))
            formatsPerStream.push(audioVideoFormats)
        }
        if (audioOnlyFormats.length > 0) {
            streamModel.push(qsTr("Audio only"))
            formatsPerStream.push(audioOnlyFormats)
        }
        if (videoOnlyFormats.length > 0) {
            streamModel.push(qsTr("Video only"))
            formatsPerStream.push(videoOnlyFormats)
        }
        if (streamModel.length == 0) {
            console.warn("No audio or video formats found")
        }
        _streamModel = streamModel
        console.log("Stream model: " + JSON.stringify(streamModel))
        _formatsPerStream = formatsPerStream
        selectActiveFormat()
    }

    function selectFormat(format) {
        formatRequested(format["index"])
    }
}
