import MiTubo 1.0

PlaylistModel {
    id: root

    function addToContinueWatching(videoData, position) {
        var list = playlistById(continueWatchingListId)
        if (!list) return

        list.addVideo(videoData, position)
    }

    function addWatchedVideo(videoData) {
        // Remove the item from the partially watched videos
        var list = playlistById(continueWatchingListId)
        if (list) {
            var index = list.indexOf(videoData)
            if (index >= 0) list.takeAt(index)
        }

        // Add video to the watch history
        list = playlistById(watchHistoryListId)
        if (list) {
            list.addVideo(videoData)
        }
    }
}
