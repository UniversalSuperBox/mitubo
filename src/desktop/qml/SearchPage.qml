import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    property string searchText: ""

    signal urlAdded(url videoUrl, string action)

    title: qsTr("Search")

    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            BackButton {
                onClicked: root.StackView.view.pop()
            }

            Label {
                text: qsTr("Search:")
            }

            TextFieldWithContextMenu {
                id: searchField
                Layout.fillWidth: true
                placeholderText: qsTr("Enter search terms")
                text: root.searchText
                selectByMouse: true
                onAccepted: root.searchText = text
            }
        }
    }

    ListView {
        anchors.fill: parent
        model: searchEngine.model
        delegate: SearchDelegate {
            pageUrl: model.url
            previewUrl: model.previewUrl
            title: model.title
            description: model.description
            duration: model.duration
            host: model.host
            width: ListView.view.width
            height: 120
            onPlayRequested: root.urlAdded(model.url, "play")
            onInfoRequested: root.urlAdded(model.url, "info")
        }

        property bool canFetchPage: true
        onVerticalOvershootChanged: {
            if (verticalOvershoot > 0) {
                if (canFetchPage) searchEngine.fetchMore();
                canFetchPage = false;
            } else {
                canFetchPage = true;
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    YandexSearch {
        id: searchEngine
        searchText: root.searchText
    }
}
