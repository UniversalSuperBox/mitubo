/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_YOUTUBE_DL_H
#define MITUBO_YOUTUBE_DL_H

#include <QJsonObject>
#include <QObject>
#include <QScopedPointer>

class QJSValue;
class QUrl;

namespace MiTubo {

class YoutubeDlPrivate;
class YoutubeDl: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool installed READ isInstalled NOTIFY isInstalledChanged)

public:
    YoutubeDl(QObject *parent = nullptr);
    ~YoutubeDl();

    bool isInstalled() const;

    Q_INVOKABLE void checkIsInstalled();

    Q_INVOKABLE void getUrlInfo(const QUrl &url, const QJSValue &callback);

Q_SIGNALS:
    void isInstalledChanged();

private:
    Q_DECLARE_PRIVATE(YoutubeDl)
    QScopedPointer<YoutubeDlPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_YOUTUBE_DL_H
