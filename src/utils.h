/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_UTILS_H
#define MITUBO_UTILS_H

#include <QObject>
#include <QString>

namespace MiTubo {

class Utils: public QObject
{
    Q_OBJECT

public:
    Utils(QObject *parent = 0);
    ~Utils();

    Q_INVOKABLE QString formatMediaTime(int milliseconds) const;
};

} // namespace

#endif // MITUBO_UTILS_H
