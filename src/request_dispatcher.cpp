/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "request_dispatcher.h"

#include <QDebug>
#include <QJsonObject>
#include <QUrl>

using namespace MiTubo;

namespace MiTubo {

class RequestDispatcherPrivate {
};

} // namespace

RequestDispatcher::RequestDispatcher(QObject *parent):
    QObject(parent),
    d_ptr(nullptr)
{
}

RequestDispatcher::~RequestDispatcher()
{
}

void RequestDispatcher::requestOpenUrl(const QUrl &url, const QString &action)
{
    Q_EMIT openUrlRequested(url, action);
}

void RequestDispatcher::requestOpenVideo(const QJsonObject &videoData,
                                         const QString &action)
{
    Q_EMIT openVideoRequested(videoData, action);
}
