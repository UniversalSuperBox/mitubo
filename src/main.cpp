/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"

#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlFileSelector>

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    app.setApplicationName("it.mardy.mitubo");
    app.setApplicationDisplayName("MiTubo");
    app.setOrganizationName(QString());
    app.setOrganizationDomain("it.mardy.mitubo");
    app.setWindowIcon(QIcon(":/icons/mitubo"));

    MiTubo::registerTypes();
    QQmlApplicationEngine engine;
    if (!qgetenv("APP_ID").isEmpty()) {
        QQmlFileSelector::get(&engine)->setExtraSelectors({"ubuntu-touch"});
    }
    engine.load(QUrl("qrc:/main.qml"));

    return app.exec();
}
