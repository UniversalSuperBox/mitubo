import qbs 1.0
import qbs.Environment

Project {
    name: "MiTubo"

    property string packageName: "it.mardy.mitubo"
    property string version: "0.1"

    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "MiTubo"
        version: project.version
        install: true

        cpp.cxxLanguageVersion: "c++14"
        cpp.defines: [
            'QT_DISABLE_DEPRECATED_BEFORE=0x050900',
            'QT_DEPRECATED_WARNINGS=0',
        ]

        Group {
            prefix: "src/"
            files: [
                "main.cpp",
                "playlist.cpp",
                "playlist.h",
                "playlist_model.cpp",
                "playlist_model.h",
                "playlist_store.cpp",
                "playlist_store.h",
                "request_dispatcher.cpp",
                "request_dispatcher.h",
                "settings.cpp",
                "settings.h",
                "types.cpp",
                "types.h",
                "utils.cpp",
                "utils.h",
                "youtube_dl.cpp",
                "youtube_dl.h",
                "youtube_dl_installer.cpp",
                "youtube_dl_installer.h",
            ]
        }

        Group {
            prefix: "src/desktop/"
            files: [
                "qml/ui.qrc",
            ]
        }

        Group {
            prefix: "data/"
            files: [
                "icons/icons.qrc",
                "mitubo.desktop",
            ]
        }

        Group {
            files: "data/mitubo.svg"
            fileTags: "freedesktop.appIcon"
        }

        Depends { name: "cpp" }
        Depends { name: "Qt.core" }
        Depends { name: "Qt.quick" }
        Depends { name: "Qt.quickcontrols2" }
        Depends { name: "Qt.svg" }
        Depends { name: "freedesktop" }

        freedesktop.name: product.name

        /*
         * Ubuntu Touch specific configuration
         */
        Depends {
            name: "ubuntutouch"
            condition: Environment.getEnv("TARGET_SYSTEM") == "UbuntuTouch"
        }

        Properties {
            condition: ubuntutouch.present
            freedesktop.desktopKeys: ubuntutouch.desktopKeys
            ubuntutouch.overrideDesktopKeys: ({
                "Icon": "./share/icons/hicolor/scalable/apps/mitubo.svg",
            })
            ubuntutouch.manifest: ({
                "version": project.version + "-0"
            })
        }

        Group {
            condition: ubuntutouch.present
            prefix: "data/ubuntu-touch/"
            files: [
                "manifest.json",
                "mitubo.apparmor",
                "mitubo.content-hub",
            ]
        }

        Group {
            condition: ubuntutouch.present
            prefix: "src/ubuntu-touch/"
            files: [
                "qml/ubuntu-touch.qrc",
            ]
        }
    }
}
