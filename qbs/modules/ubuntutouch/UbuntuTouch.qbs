import qbs
import qbs.FileInfo
import qbs.ModUtils
import qbs.TextFile

Module {
    id: ubuntutouchModule

    readonly property string clickArchitecture: {
        switch (qbs.architecture) {
        case "armv7a":
            return "armhf";
        default:
            return qbs.architecture;
        }
    }

    readonly property var defaultManifest: {
        return {
            'name': project.packageName,
            'title': product.name,
            'framework': "ubuntu-sdk-16.04",
            'architecture': clickArchitecture,
            'version': project.version,
        }
    }

    readonly property var defaultDesktopKeys: ({
        "Exec": FileInfo.joinPaths(qbs.installPrefix,
                                   product.installDir,
                                   product.targetName),
        "X-Ubuntu-Touch": "true",
    })

    property var overrideDesktopKeys: ({})

    readonly property var desktopKeys: {
        var ret = defaultDesktopKeys;
        for (var key in overrideDesktopKeys) {
            ret[key] = overrideDesktopKeys[key];
        }
        return ret;
    }

    FileTagger {
        patterns: [
            "*.accounts",
            "*.apparmor",
            "*.content-hub",
            "*.url-dispatcher",
        ]
        fileTags: ["ubuntutouch.clickfile"]
    }

    additionalProductTypes: [ "ubuntutouch.clickfile" ]

    FileTagger {
        patterns: [ "manifest.json" ]
        fileTags: [ "ubuntutouch.manifest_source" ]
    }

    Rule {
        inputs: [ "ubuntutouch.manifest_source" ]
        outputFileTags: [ "ubuntutouch.manifest", "ubuntutouch.clickfile" ]
        outputArtifacts: [
            {
                fileTags: [ "ubuntutouch.manifest", "ubuntutouch.clickfile" ],
                filePath: input.fileName
            }
        ]

        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = input.fileName + "->" + output.fileName;
            cmd.manifest = ModUtils.moduleProperty(product, "manifest") || {}
            cmd.highlight = "codegen";
            cmd.sourceCode = function() {
                var file = new TextFile(input.filePath);
                var content = file.readAll();
                file.close();
                var originalManifest = JSON.parse(content);
                for (key in manifest) {
                    if (manifest.hasOwnProperty(key)) {
                        originalManifest[key] = manifest[key];
                    }
                }

                var defaultManifest = ModUtils.moduleProperty(product, "defaultManifest");
                for (key in defaultManifest) {
                    if (defaultManifest.hasOwnProperty(key) && !(key in originalManifest)) {
                        originalManifest[key] = defaultManifest[key];
                    }
                }

                file = new TextFile(output.filePath, TextFile.WriteOnly);
                file.write(JSON.stringify(originalManifest, null, 2));
                file.close();
            }
            return [cmd];
        }
    }

    Group {
        fileTagsFilter: ["ubuntutouch.clickfile"]
        qbs.install: true
    }
}
